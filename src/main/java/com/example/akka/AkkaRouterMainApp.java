package com.example.akka;

import com.example.akka.router.MessageActor;
import com.example.akka.router.RouterActor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.FromConfig;
import akka.routing.RoundRobinPool;

public class AkkaRouterMainApp {

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.apply("akkaRouter");

        // ActorRef router = system.actorOf(Props.create(RouterActor.class));
        ActorRef router = system.actorOf(FromConfig.getInstance().props(Props.create(MessageActor.class)), "router1");
        // ActorRef router = system.actorOf(new RoundRobinPool(5).props(Props.create(MessageActor.class)), "development");

        for (int i = 0; i < 100; i++) {

            router.tell(String.format(
                "hello from main pages : [%s]", i)
                , ActorRef.noSender());
        }

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}