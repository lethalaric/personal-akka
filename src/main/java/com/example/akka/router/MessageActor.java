package com.example.akka.router;

import akka.actor.AbstractLoggingActor;

/**
 * MessageActor
 */
public class MessageActor extends AbstractLoggingActor {

    @Override
    public Receive createReceive() {
        // TODO Auto-generated method stub
        return receiveBuilder()
        .match(String.class, this::messageReceived)
        .build();
    }

    private void messageReceived(String message)
    {
        log().info(message);
    }
}