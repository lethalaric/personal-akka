package com.example.akka.router;

import java.util.ArrayList;
import java.util.List;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;

public class RouterActor extends AbstractLoggingActor {

    private Router router;

    // {
    //     final List<Routee> routees = new ArrayList<>();
    //     for (int i = 0; i < 5; i++)
    //     {
    //         final ActorRef actorRef = getContext().actorOf(Props.create(MessageActor.class));
    //         getContext().watch(actorRef);

    //         routees.add(new ActorRefRoutee(actorRef));
    //     }

    //     router = new Router(new RoundRobinRoutingLogic(), routees);
    // }

    @Override
    public Receive createReceive() {
        // TODO Auto-generated method stub
        return receiveBuilder()
        .match(String.class, this::routing)
        .build();
    }

    private void routing(String message)
    {
        router.route(message, getSender());
    }
    
}